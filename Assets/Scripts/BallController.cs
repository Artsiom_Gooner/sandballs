﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallController : MonoBehaviour
{
    [SerializeField] private Transform _ballSpawnTransfrom;
    [SerializeField] private Ball _ballPrefab;

    private Ball _ball;

    public void CreateBall()
    {
        if (_ball != null)
        {
            Destroy(_ball.gameObject);
        }

        _ball = Instantiate(_ballPrefab, transform);
        _ball.transform.position = _ballSpawnTransfrom.position;

        CameraController.Instance.SetBallFollowTransform(_ball.transform);
    }
}
