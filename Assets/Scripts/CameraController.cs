﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviourSingleton<CameraController>
{
    private Transform _ballFollowTransform;

    private void LateUpdate()
    {
        gameObject.transform.SetPosY(_ballFollowTransform.position.y);
    }

    public void SetBallFollowTransform(Transform ballTransform)
    {
        _ballFollowTransform = ballTransform;
    }
}
