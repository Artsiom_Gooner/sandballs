﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ball : MonoBehaviour
{
    private Rigidbody _rigidBody;

    public static event Action<bool> OnBallReachTarget;

    private void Start()
    {
        _rigidBody = GetComponentInParent<Rigidbody>();
        _rigidBody.Sleep();
    }

    private void Update()
    {
        if (transform.position.y <= -70f)
        {
            BallReachTarget(false);
            Destroy(this.gameObject);
        }
    }

    public void BallReachTarget(bool res)
    {
        OnBallReachTarget?.Invoke(res);
    }
}
