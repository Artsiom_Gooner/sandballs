﻿using UnityEngine;

public abstract class GUIScreen : MonoBehaviour
{
    [SerializeField] private ScreenType _screenType;
    public ScreenType ScreenType
    {
        get
        {
            return _screenType;
        }
    }
}