﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class GameScreen : GUIScreen, IPointerDownHandler, IPointerUpHandler, IDragHandler
{
    [SerializeField] private Text _resultText;

    private PointerEventData _prevEventData;

    private void Start()
    {
        _resultText.gameObject.SetActive(false);
    }

    private void OnEnable()
    {
        Ball.OnBallReachTarget += Ball_OnBallReachTarget;
    }

    private void OnDisable()
    {
        Ball.OnBallReachTarget -= Ball_OnBallReachTarget;
    }

    private void Ball_OnBallReachTarget(bool res)
    {
        ShowResult(res);
    }

    private void LateUpdate()
    {
        if (_prevEventData != null)
        {
            Vector2 currentPos = _prevEventData.position;
            Vector2 prevPos = currentPos - _prevEventData.delta;
            PressSand(prevPos, currentPos);
        }
    }

    public void ShowResult(bool result)
    {
        _resultText.text = result ? "Win" : "Fail";
        _resultText.gameObject.SetActive(true);
    }

    public void OnDrag(PointerEventData eventData)
    {
        _prevEventData = eventData;
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        _prevEventData = eventData;
        PressSand(eventData.position, eventData.position);
    }

    private void PressSand(Vector3 start, Vector3 end)
    {
        RaycastHit hit;
        Ray rayStart = Camera.main.ScreenPointToRay(start);
        Ray rayEnd = Camera.main.ScreenPointToRay(end);
        Transform hitTransformStart = null;
        Transform hitTransformEnd = null;

        if (Physics.Raycast(rayStart, out hit))
        {
            hitTransformStart = hit.transform;
        }

        if (Physics.Raycast(rayEnd, out hit))
        {
            hitTransformEnd = hit.transform;
        }

        if (hitTransformStart != null && hitTransformEnd != null)
        {
            GameController.Instance.PressSand(hitTransformStart.position, hitTransformEnd.position);
        }
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        _prevEventData = null;
    }
}
