﻿using System;
using UnityEngine;

public class GameController : MonoBehaviourSingleton<GameController>
{
    [SerializeField] private CameraController _cameraController;
    [SerializeField] private SandController _sandController;
    [SerializeField] private BallController _ballController;

    public bool IsGameStarted { get; set; }

    public static event Action OnGameStarted;
    public static event Action OnGameFinished;

    private void Start()
    {
        Application.targetFrameRate = 300;

        IsGameStarted = false;
        StartGame();
    }

    public void PressSand(Vector3 start, Vector3 end)
    {
        _sandController.PressSand(start, end);
    }

    public void StartGame()
    {
        _sandController.CreateSand();
        _ballController.CreateBall();

        IsGameStarted = true;
        OnGameStarted?.Invoke();
    }

    public void FinishGame()
    {
        OnGameFinished?.Invoke();
    }
}
