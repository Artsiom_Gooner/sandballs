﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallHitChecker : MonoBehaviour
{
    private void OnCollisionEnter(Collision collision)
    {
        Ball ball = collision.collider.GetComponent<Ball>();

        if (ball != null)
        {
            ball.BallReachTarget(true);
        }
    }
}
