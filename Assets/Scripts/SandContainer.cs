﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SandContainer : MonoBehaviour
{
    [SerializeField] private SandPlane _sandPlanePrefab;
    [SerializeField] private int _planeWidthCount = 15;
    [SerializeField] private int _planeHeightCount = 25;

    private float _planeScale = 10;
    private List<SandPlane> _sandPlaneList = new List<SandPlane>();

    public void FillSandContainer()
    {
        if (_sandPlaneList != null)
        {
            foreach (SandPlane plane in _sandPlaneList)
            {
                Destroy(plane.gameObject);
            }
        }

        Vector3 planePos = Vector3.zero;
        for (int i = 0; i < _planeWidthCount; i++)
        {
            planePos.x = i * _sandPlanePrefab.transform.localScale.x * _planeScale;
            for (int j = 0; j < _planeHeightCount; j++)
            {
                planePos.y = -j * _sandPlanePrefab.transform.localScale.y * _planeScale;
                SandPlane plane = Instantiate(_sandPlanePrefab, transform);
                plane.transform.position = planePos;

                _sandPlaneList.Add(plane);
            }
        }
    }

    public void PressPlaneVertices(List<(SandPlane, List<int>)> planeVertices)
    {
        foreach ((SandPlane, List<int>) plane in planeVertices)
        {
            SandPlane sandPlane = _sandPlaneList.Find(x => x == plane.Item1);
            if (sandPlane != null)
            {
                MeshFilter meshFilter = sandPlane.GetComponentInParent<MeshFilter>();
                Mesh mesh = meshFilter.sharedMesh;
                Vector3[] vertices = new Vector3[mesh.vertices.Length];

                for (int i = 0; i < mesh.vertices.Length; i++)
                {
                    vertices[i] = mesh.vertices[i];
                    if (plane.Item2.Contains(i))
                    {
                        int index = plane.Item2.IndexOf(i);
                        Vector3 worldPos = sandPlane.transform.TransformPoint(vertices[i]);

                        worldPos.z = 0.5f;

                        Vector3 localPos = sandPlane.transform.InverseTransformPoint(worldPos);
                        vertices[i] = localPos;
                    }
                }

                mesh.vertices = vertices;
                mesh.RecalculateBounds();

                var ball = FindObjectOfType<Ball>();
                var rb = ball.GetComponent<Rigidbody>();
                if (rb.IsSleeping())
                {
                    rb.WakeUp();
                }
                mesh.RecalculateNormals();
                mesh.RecalculateTangents();

                MeshCollider meshCollider = sandPlane.GetComponentInParent<MeshCollider>();
                meshCollider.sharedMesh = mesh;
            }
        }
    }
}
