﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SandPlane : MonoBehaviour
{
    private MeshFilter _meshFilter;
    private MeshCollider _meshCollider;
    private Mesh _mesh;

    void Start()
    {
        _meshFilter = GetComponent<MeshFilter>();
        _meshCollider = GetComponent<MeshCollider>();
        _meshFilter.sharedMesh = _meshFilter.mesh;
        _mesh = _meshFilter.sharedMesh;
        _meshCollider.sharedMesh = _mesh;
    }
}
