﻿using UnityEngine;

public static class TransformExtensions
{
    public static void SetPosX(this Transform transform, float x)
    {
        transform.position = new Vector3(x, transform.position.y, transform.position.z);
    }

    public static void SetPosY(this Transform transform, float y)
    {
        transform.position = new Vector3(transform.position.x, y, transform.position.z);
    }

    public static void SetPosZ(this Transform transform, float z)
    {
        transform.position = new Vector3(transform.position.x, transform.position.y, z);
    }

    public static void SetPosXY(this Transform transform, float x, float y)
    {
        transform.position = new Vector3(x, y, transform.position.z);
    }

    public static void SetPosXZ(this Transform transform, float x, float z)
    {
        transform.position = new Vector3(x, transform.position.y, z);
    }

    public static void SetPosYZ(this Transform transform, float y, float z)
    {
        transform.position = new Vector3(transform.position.x, y, z);
    }

    public static void SetPosXYZ(this Transform transform, float x, float y, float z)
    {
        transform.position = new Vector3(x, y, z);
    }

    public static void SetScaleX(this Transform transform, float x)
    {
        transform.position = new Vector3(x, transform.position.y, transform.position.z);
    }

    public static void SetScaleY(this Transform transform, float y)
    {
        transform.localScale = new Vector3(transform.localScale.x, y, transform.localScale.z);
    }

    public static void SetScaleZ(this Transform transform, float z)
    {
        transform.localScale = new Vector3(transform.localScale.x, transform.localScale.y, z);
    }

    public static void SetScaleXY(this Transform transform, float x, float y)
    {
        transform.localScale = new Vector3(x, y, transform.localScale.z);
    }

    public static void SetScaleXZ(this Transform transform, float x, float z)
    {
        transform.localScale = new Vector3(x, transform.localScale.y, z);
    }

    public static void SetScaleYZ(this Transform transform, float y, float z)
    {
        transform.localScale = new Vector3(transform.localScale.x, y, z);
    }

    public static void SetScaleXYZ(this Transform transform, float x, float y, float z)
    {
        transform.localScale = new Vector3(x, y, z);
    }
}
