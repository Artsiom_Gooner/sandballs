﻿using System;
using System.Collections.Generic;
using UnityEngine;

public static class GameObjectExtensions
{
    public static IEnumerator<GameObject> MoveToPoint(this GameObject gameObject, Vector3 target, float duration, AnimationCurve curve = null, Action onComplete = null)
    {
        Vector3 startPosition = gameObject.transform.position;
        float currentTime = 0.0f;

        while (currentTime < duration)
        {
            float factor = curve != null ? curve.Evaluate(currentTime / duration) : currentTime / duration;
            gameObject.transform.position = Vector3.Lerp(startPosition, target, factor);

            currentTime += Time.deltaTime;
            yield return null;
        }

        gameObject.transform.position = Vector3.Lerp(startPosition, target, 1.0f);

        onComplete?.Invoke();
    }

    public static IEnumerator<GameObject> RotateToAngle(this GameObject gameObject, Quaternion eulerAngle, float duration, AnimationCurve curve = null, Action onComplete = null)
    {
        Quaternion startRotation = gameObject.transform.rotation;
        float currentTime = Time.deltaTime;

        while (currentTime < duration)
        {
            float factor = curve != null ? curve.Evaluate(currentTime / duration) : currentTime / duration;
            gameObject.transform.rotation = Quaternion.Lerp(startRotation, eulerAngle, factor);

            currentTime += Time.deltaTime;
            yield return null;
        }

        gameObject.transform.rotation = Quaternion.Lerp(startRotation, eulerAngle, 1.0f);

        onComplete?.Invoke();
    }

    public static IEnumerator<GameObject> ScaleToSize(this GameObject gameObject, Vector3 size, float duration, AnimationCurve curve = null, Action onComplete = null)
    {
        Vector3 startScale = gameObject.transform.localScale;
        float currentTime = 0.0f;

        while (currentTime < duration)
        {
            float factor = curve != null ? curve.Evaluate(currentTime / duration) : currentTime / duration;
            gameObject.transform.localScale = Vector3.Lerp(startScale, size, factor);

            currentTime += Time.deltaTime;
            yield return null;
        }

        gameObject.transform.localScale = Vector3.Lerp(startScale, size, 1.0f);

        onComplete?.Invoke();
    }
}
