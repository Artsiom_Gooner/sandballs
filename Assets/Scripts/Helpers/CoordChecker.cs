﻿using UnityEngine;

public static class CoordChecker
{
    public static bool CheckIfCoordInCircle(Vector3 point, Vector3 center, float radius)
    {
        float xDiff = Mathf.Abs(point.x - center.x);
        float yDiff = Mathf.Abs(point.y - center.y);

        if (xDiff > radius || yDiff > radius)
        {
            return false;
        }

        if (xDiff + yDiff <= radius)
        {
            return true;
        }

        return Mathf.Pow(xDiff, 2) + Mathf.Pow(yDiff, 2) <= Mathf.Pow(radius, 2);
    }

    public static bool CheckIfCoordInCapsuleByTriangles(Vector3 point, Vector3 start, Vector3 end, float radius)
    {
        bool inStartCircleExists = CheckIfCoordInCircle(point, start, radius);
        bool inEndCircleExists = CheckIfCoordInCircle(point, end, radius);

        if (inStartCircleExists || inEndCircleExists)
        {
            return true;
        }

        float magnitude = (end - start).magnitude;
        if (magnitude < 1.0f)
        {
            return false;
        }

        Vector3 distance = end - start;
        Vector3 ortDist = new Vector3(distance.y, -distance.x);
        Vector3 ortDistFinal = ortDist.normalized * radius;

        Vector3 rectPointA = start - ortDistFinal;
        Vector3 rectPointB = start + ortDistFinal;
        Vector3 rectPointC = end + ortDistFinal;
        Vector3 rectPointD = end - ortDistFinal;

        float distAB = Mathf.Sqrt(Mathf.Pow(rectPointA.x - rectPointB.x, 2) + Mathf.Pow(rectPointA.y - rectPointB.y, 2));
        float distBC = Mathf.Sqrt(Mathf.Pow(rectPointB.x - rectPointC.x, 2) + Mathf.Pow(rectPointB.y - rectPointC.y, 2));
        float distCD = Mathf.Sqrt(Mathf.Pow(rectPointC.x - rectPointD.x, 2) + Mathf.Pow(rectPointC.y - rectPointD.y, 2));
        float distDA = Mathf.Sqrt(Mathf.Pow(rectPointD.x - rectPointA.x, 2) + Mathf.Pow(rectPointD.y - rectPointA.y, 2));

        float distAP = Mathf.Sqrt(Mathf.Pow(rectPointA.x - point.x, 2) + Mathf.Pow(rectPointA.y - point.y, 2));
        float distBP = Mathf.Sqrt(Mathf.Pow(rectPointB.x - point.x, 2) + Mathf.Pow(rectPointB.y - point.y, 2));
        float distCP = Mathf.Sqrt(Mathf.Pow(rectPointC.x - point.x, 2) + Mathf.Pow(rectPointC.y - point.y, 2));
        float distDP = Mathf.Sqrt(Mathf.Pow(rectPointD.x - point.x, 2) + Mathf.Pow(rectPointD.y - point.y, 2));

        float rectS = distAB * distBC;

        float semiP1 = (distAB + distAP + distBP) / 2;
        float semiP2 = (distBC + distBP + distCP) / 2;
        float semiP3 = (distCD + distCP + distDP) / 2;
        float semiP4 = (distDA + distDP + distAP) / 2;

        float trS1 = Mathf.Sqrt(semiP1 * (semiP1 - distAB) * (semiP1 - distAP) * (semiP1 - distBP));
        float trS2 = Mathf.Sqrt(semiP2 * (semiP2 - distBC) * (semiP2 - distBP) * (semiP2 - distCP));
        float trS3 = Mathf.Sqrt(semiP3 * (semiP3 - distCD) * (semiP3 - distCP) * (semiP3 - distDP));
        float trS4 = Mathf.Sqrt(semiP4 * (semiP4 - distDA) * (semiP4 - distDP) * (semiP4 - distAP));

        float sumTrS = trS1 + trS2 + trS3 + trS4;

        if (Mathf.Approximately(sumTrS, rectS))
        {
            return true;
        }
        else if (sumTrS > rectS)
        {
            return false;
        }


        return false;
    }

    public static bool CheckIfCoordInCapsule(Vector3 point, Vector3 start, Vector3 end, float radius)
    {
        Vector3 endStart = end - start;
        Vector3 pointStart = point - start;
        float startEndDist = Vector3.Distance(end, start);

        bool inStartCircleExists = CheckIfCoordInCircle(point, start, radius);
        bool inEndCircleExists = CheckIfCoordInCircle(point, end, radius);

        if (inStartCircleExists || inEndCircleExists)
        {
            return true;
        }

        if (startEndDist < 1.0f)
        {
            return false;
        }

        Vector3 project = Vector3.Project(pointStart, endStart);
        Vector3 projectPoint = start + project;

        float startProjectDist = Vector3.Distance(projectPoint, start);
        float endProjectDist = Vector3.Distance(projectPoint, end);

        if (Mathf.Approximately(startProjectDist + endProjectDist, startEndDist))
        {
            float projectDist = Vector3.Distance(point, projectPoint);
            if (projectDist <= radius)
            {
                return true;
            }
        }

        return false;
    }
}
