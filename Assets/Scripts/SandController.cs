﻿using System.Collections.Generic;
using UnityEngine;

public class SandController : MonoBehaviourSingleton<SandController>
{
    [SerializeField] private Transform _sandContainerSpawnTransform;
    [SerializeField] private SandContainer _sandContainer;
    [SerializeField] private float _holeRadius = 1.5f;
    [SerializeField] private LayerMask _sandLayerMask;

    private List<(SandPlane, List<int>)> _pressedPlaneVertices = new List<(SandPlane, List<int>)>();

    public void CreateSand()
    {
        _sandContainer.FillSandContainer();
        _sandContainer.transform.position = _sandContainerSpawnTransform.position;
    }

    public void PressSand(Vector3 start, Vector3 end)
    {
        _pressedPlaneVertices = new List<(SandPlane, List<int>)>();
        Collider[] colliders = Physics.OverlapCapsule(start, end, _holeRadius, _sandLayerMask.value);

        for (int i = 0; i < colliders.Length; i++)
        {
            SandPlane plane = colliders[i].GetComponent<SandPlane>();
            if (plane != null)
            {
                MeshFilter meshFilter = plane.GetComponentInParent<MeshFilter>();
                Mesh mesh = meshFilter.mesh;
                List<int> vertices = new List<int>();

                for (int j = 0; j < mesh.vertices.Length; j++)
                {
                    Vector3 worldPos = plane.transform.TransformPoint(mesh.vertices[j]);
                    bool coordInCapsule = CoordChecker.CheckIfCoordInCapsule(worldPos, start, end, _holeRadius);

                    if (coordInCapsule)
                    {
                        vertices.Add(j);
                    }
                }

                _pressedPlaneVertices.Add((plane, vertices));
            }
        }

        _sandContainer.PressPlaneVertices(_pressedPlaneVertices);
    }
}
